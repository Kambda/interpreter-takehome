# Setup
The project consists of a single Python 2 script. In order to be able to run the script,
first install the requirements:

**Note: This steps asume you have a working python 2 installation with pip package installed.**

    $ pip install -r requirements.txt

# Running the script
Once the requirements are installed, simply run the script with the following command:

    $ python app.py

The script will loop into the schema files in `./schemas`, search for each corresponding
data file in `./data` and process each file.

# Technical Specifications
The task was done in Python 2 as a single script. I chose Python instead of JavaScript
just for simplicity. Python already includes built-in tools to work with files and
folders, parsing and dumping CSV and reading regular text files. In order to send
http requests a single package needs to be installed: `requests`.

The script is split into 5 functions. One main entry point `process` looks for
schema files and 4 other functions that perform specific tasks. The idea is to keep
the logic separated by concerns, it makes the code easier to read and maintain.   

## Python 2 vs Python 3
There's no special requirement or technical reason here, I chose Python 2 just because
it's more common and it's already pre-installed in most linux distributions.

## Possible other approaches
To be honest, a Python 2 script was the first approach I considered. But this could
have been done in JavaScript.

## Bonus question
In order to ensure that the contents of the API's database match the data files ingested
by the script we could make the API reply with a JSON object with the record data that
was updated in case of success, or a failure message in case it didn't match any record
or an error ocurred. The script would then evaluate each reply and take actions
accordingly.