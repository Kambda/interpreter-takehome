# EchoEcho takehome project

### Background

Medicare doctors are required to submit performance data for a number of "quality measures".
Quality measures have titles, identifiers and other attributes. This data is ultimately used
to assess quality of care and compensate doctors appropriately.

Your application will submit doctor’s performance data to an API.
This application should handle two sets of files: data files and schema files. Using Python or Javascript,
write an application that will ingest these data files according to the schemas laid
out in the schema files and post the contents as requests to an external system.

### Deliverables

Please send the following in email to your EchoEcho point of contact.
1. Part 1: zipped file or git repository (preferred) or a link to where your code is hosted
1. Part 2: PDF or text document

Please do not spend more than 4 hours on this exercise. We're looking for correctness first, along
with code that is simple and easy to understand. We may also ask you to build on this code
during a phone interview follow up, so keep that in mind!

## Part 1: Implementation

Your application should expect data files to be in a folder "data/" relative to your application and
schema files to be in a folder "schemas/" relative to your application. You can find some example
data and schema files below.

Data files will have filenames equal to their file format type and a ".txt" extension while schema
files will have a ".csv" extension. So "booleanmeasures.csv" would be the schema for the data file
"booleanmeasures.txt".

Data files will be flat text files with rows matching single records for the database. Schema files
will be csv formatted with columns:

1. name: name of that column in the database table
1. width: characters used by the column in the data file
1. datatype: SQL data type that should be used to store the value in the database table.

Acceptable values are TEXT, INTEGER and BOOLEAN.

1. Every csv will have a measure_id TEXT column

Each row of the data file should be sent as a JSON blob, formatted as specified by the matching
schema file, in a POST request to this external system (this endpoint is just a placeholder at this time):

https://jsonplaceholder.typicode.com/posts

You will build upon this exercise during the followup interview, so design and implement
accordingly. Please also include a README with instructions for how to run your code.

#### Example

Example contents inside ./schemas/booleanmeasures.csv
measure_id,10,TEXT
performance_year,4,INTEGER
is_required,1,BOOLEAN
minimum_score,2,INTEGER

Example contents inside ./data/booleanmeasures.txt
IA_PCMH   20171 0
ACI_LVPP  20170-1
CAHPS_1   2017010

Sample JSON body: This should be sent in the body of your 1st POST request to the external
system.

{
 "measure_id": "IA_PCMH",
 "performance_year": 2017,
 "is_required": true,
 "minimum_score": 0
}

and your second POST will look like this:

{
 "measure_id": "ACI_LVPP",
 "performance_year": 2017,
 "is_required": false,
 "minimum_score": -1
}

The application should create post requests for each line of each text file in the /data directory which
has an accompanying schema file in the /schema directory.
Files inside either the data or schema directories which do not have a match can be ignored.

## Part 2: Technical Specification

Please write and include a short specification document which describes why you chose the approach you did
and possible other approaches you considered.

Bonus question: If we are required to ensure that the contents of the API's database match the data files ingested
by your application. Describe how you would approach this requirement.

