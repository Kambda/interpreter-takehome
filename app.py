import csv
import json
import requests

from os import listdir, path
from os.path import isfile, join

API_ENDPOINT = 'https://jsonplaceholder.typicode.com/posts'


def process():
    schema_files_dir = './schemas'
    data_files_dir = './data'
    for schema_file in get_files(schema_files_dir):
        file_name, schema_file_extension = path.splitext(schema_file)
        if schema_file_extension.lower() == '.csv':
            data_file_path = data_files_dir + '/' + file_name + '.txt'
            if file_exists(data_file_path):
                schema_file_path = schema_files_dir + '/' + schema_file
                schema_columns = process_schema(schema_file_path)
                process_data_file(data_file_path, schema_columns)
            else:
                print('Ignoring ' + schema_file + '. No corresponding data file.')


def file_exists(file_path):
    result = False
    try:
        f = open(file_path)
        f.close()
        result = True
    except IOError:
        print('File ' + file_path + ' not found.')

    return result


def get_files(dir_path):
    return [f for f in listdir(dir_path) if isfile(join(dir_path, f))]


def process_schema(schema_path):
    allowed_data_types = [
        'TEXT',
        'INTEGER',
        'BOOLEAN',
    ]
    with open(schema_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        columns = []
        for row in csv_reader:
            column = {
                'name': row[0],
                'width': row[1],
                'datatype': row[2],
            }
            if column['datatype'] in allowed_data_types:
                columns.append(column)
        return columns


def process_data_file(data_path, schema_columns):
    with open(data_path) as data_file:
        print('Processing ' + data_path)
        for data_line in data_file:
            data = {}
            i = 0
            for column in schema_columns:
                col_width = int(column['width'])
                col_type = column['datatype']
                col_name = column['name']

                value = data_line[i:i+col_width].strip()
                i += col_width

                if col_type == 'INTEGER':
                    value = int(value)
                elif col_type == 'BOOLEAN':
                    value = bool(int(value))

                data[col_name] = value
            r = requests.post(url=API_ENDPOINT, data=json.dumps(data)) # Send the data as a JSON string
            # r = requests.post(url=API_ENDPOINT, data=data) # Send the data as a dictionary
            print(r.text)


if __name__ == '__main__':
    process()
